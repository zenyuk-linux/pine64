install fedora to emmc memory (internal)
==
download jumpdrive from https://github.com/dreemurrs-embedded/Jumpdrive/
howto: https://github.com/dreemurrs-embedded/Jumpdrive
howto (general): https://wiki.pine64.org/index.php?title=PinePhone_Installation_Instructions#Reuse_SD_card_for_data_storage_on_system_booting_from_eMMC

insert flash drive into pc using card reader

use firefox on gnome to initiate flash by clicking downloaded img.xz file (select the flash drive)
or
use dd to flash the image to an SD card. Jumpdrive is pretty small, so there is no need for a large storage SD card.

Insert the SD card to the device, then boot it up, you should get a nice splash screen and you should see a new storage device after you plug the device to USB.

Download Fedora from: https://quarantine.ocf.berkeley.edu/pp-fedora-20200615.img.zst
Extract the file. If you don't know how, use zstd like zstd -d $FILENAME.
Select Phone's eMMC memory (for me /dev/sdc) Flash with dd
Expand the partition on your PC, the phone doesn't yet do it automatically on boot.
    Quick guide on how to expand a partition in Fedora:
    sudo parted /dev/<your_sd_card_device>
    (parted) resizepart 2 100%
    (parted) quit
    sudo resize.f2fs /dev/<the_second_sd_card_PARTITION>
Connect to WiFi using phosh settings.
The default user/pass is pine/1111.

disconnect the PinePhone from your PC, power it down
remove the Jumpdrive SD card
boot into your OS of choice on eMMC

reuse SD card
check the device of your SD card
lsblk
as an example lets assume it is /dev/mmcblk0 then below will clear the relevant sectors of your card
sudo dd if=/dev/zero of=/dev/mmcblk0 bs=8k seek=1 count=4
